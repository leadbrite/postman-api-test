### Setup ###

Do an npm install to install newman. It is included on package.json file.

`npm install`


### Setup test environment file ###

1. Copy `environments/PagesAPITestEnv.template` into `environments/APITestEnv.json`
2. Modify the following variables on `environments/APITestEnv.json`


  > `NEWMAN_STARGATE` - Stargate test URL: https://stargate-dot-leadpage-test.appspot.com.

  > `NEWMAN_BRIDGE` - Our default test url: https://my.leadpagestest.net

  > `NEWMAN_USERNAME` - Username.

  > `NEWMAN_PASSWORD` - Password.

  > `NEWMAN_TEMPLATE_ID` - The template id for testing: 5790228451164160

  > `NEWMAN_MAILCHIMP_TOKEN` - Our MailChimp Token


### Test execution ###

1. Make sure you’re up to date with the repo
`git pull`

2. Execute the test from /postman-api-test/:

  > `python run_collections.py` - runs all collections.

  > `python run_collections.py -c Assets Pages` - runs the specific collection/s that is specified on the `-c` parameter.

(Note: You can specify your test environment using `-e` parameter and defaults to `APITestEnv.json` if none.)


### Framework Structure ###

1. `/collections` - a directory for our API test suites.
2. `/environments` - a directory for our test environments.
3. `/results` - a directory where the test results are saved after execution. Test results are overwritten after each run.

### Config file ###
`test_config.py` - contains our available test suites and their filename as a dictionary. When adding new collections, this file should be updated also to register the collection.
