from test_config import *
from newman_util import *
import argparse

def run(collections, env):
    """Runs each of the specified collections

    :param dict collections: The API test suites to run. If empty, run all collections
    :param str env: The test environment to use
    """
    if collections:
        for collection in collections:
            collection = collection.title()
            api_suite = suites[collection]
            result = collection + 'APISuiteResult.xml'
            run_newman_test(api_suite, env, result)
    else:
        for suite_key, api_suite in suites.items():
            result = suite_key + 'APISuiteResult.xml'
            run_newman_test(api_suite, env, result)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="run API tests")
    parser.add_argument('-c', '--collections', nargs='*', help='API test suite to run e.g. Assets Pages')
    parser.add_argument('-e', '--env', nargs='?', help='Filename of your environment file under /environments folder')
    parser.add_argument('-u', '--url', nargs='?', help='Your versioned test url')
    args = parser.parse_args()
    if verify_arguments(args):
        if not args.collections:
            collections = {}
        else:
            collections = args.collections
        if not args.env:
            env = 'APITestEnv.json'
        else:
            env = args.env
        if args.url:
            update_test_url(args.url)

        run(collections, env)
