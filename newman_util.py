from test_config import *
import inspect
import os
import subprocess
import json

me = inspect.getabsfile(inspect.currentframe())
test_dir = os.path.dirname(me)

def run_newman_test(api_suite, env, result):

    """Runs the newman test with its shell command

    :param str api_suite: Filename of the test suite on /collections directory
    :param str env: Filename of the test environment on /environments directory
    :param str result: A filename to use when the results are saved on /results directory
    """

    command = 'node_modules/newman/bin/newman -c ' + test_dir + '/collections/' + api_suite + ' -e ' + test_dir + '/environments/' + env + ' -t ' + test_dir + '/results/' + result + ' -y 300 -S'
    test_proc = subprocess.Popen(command, shell=True)
    test_proc.wait()

def verify_arguments(args):
    """Verifies the argument the the user provided when running the test

    :param list agrs: a list of data from command line arguments

    :return boolean flag: return true if all arguments are valid
    """

    flag = True
    if args.collections:
        collections = args.collections
        for collection in collections:
            collection = collection.title()
            if collection not in suites:
                flag = False
                print_error_message("Specified API test suite not found on test_config.py: " + collection)
    if args.env:
        if not os.path.exists(test_dir + '/environments/' + args.env):
            flag = False
            print_error_message("Specified environment file does not exist: " + test_dir + "/environments/" + args.env)
    return flag

def print_error_message(msg):
    """Prints a red error message on console.
    """
    print "\033[91m"
    print msg
    print "\033[0m"

def update_test_url(url):
    env_template_text = open('environments/APITestEnv.json','r').read()
    f = open('environments/APITestEnv.json','w')
    env_template_text = env_template_text.replace('https://my.leadpagestest.net', url)
    f.write(env_template_text)
