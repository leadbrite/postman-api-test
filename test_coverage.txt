ASSETS API
  Location: collections/AssetsAPISuite.json
  Tests:
    1. Create A Reference Asset
        - Verifies if a reference asset can be created. Deletes afterwards and
          verify if the newly created asset can not be retrieved.

    2. Upload and Delete Asset
        - Verifies if the API can upload an asset. This uses a multipart/form-data
          for upload. Deletes the uploaded file afterwards and verify if the
          newly uploaded asset can not be retrieved.

    3. Upload Multiple Files
        - Verifies if the API can upload multiple asset in one request. This uses a
          multipart/form-data for upload.

    4. Verify Asset Permission
        - Verifies if the uploaded asset is just available to the owner.
        - Logs in and uploads an asset then logs out after the upload is successful.
        - Logs in another user and tries to access the newly upload asset from the
          previous user. After verification, the previous user is logged in and
          deletes the file.

    5. View Asset List
        - Verifies if the assets can be retrieved as a list.

    6. Negative Test
        - This test verifies the error messages and restrictions when ever a bad
        request is sent to the API.
        - Negative test includes:
          401 - Unauthorized request
          404 - Not found
          400 - Malformed data upload
          413 - File too large


Pages API
  Location: collections/PagesAPISuite.json
  Tests:
    1. Access Pages without Auth
        - Verifies if the response code is 401(User should not be authorized) When
          accessing the page list.
        - Verifies if the response code is 401(User should not be authorized) When
          creating a page.

    2. Create Page From Template
        - Creates a page using a template id and validates proper share page functionality.
        - Publishes the page using `action` parameter and verifies if page
        information/response body has been updated.

    3. Duplicate Existing LeadPage
        - Verifies if the LeadPage can be duplicated. Creates an original page
        and duplicates it after.

    4. Name and Slug Validation
        - Verifies if the page can not create a page with a duplicate Slug and Name.

    5. Publish Options
        - Verifies if the redirect options are available.
        - Verifies if redirecting the page without a Target returns an error and
          its corresponding message. Request must be rejected.
        - Verifies if redirecting the page with an invalid URL returns an error and
          its corresponding message. Request must be rejected.
        - Verifies if redirecting the page with valid URL is accepted.
          with a valid URL, this verifies if `redirectActive` property is set to true.
        - This also verifies if `Redirect Desktop Only` feature is accepted. Also verifies
          if `redirectMobile` is set to false.

    6. Negative Test
        - This test verifies the error messages and restrictions when ever a bad
        request is sent to the API.
        - Negative test includes:
            401 - Unauthorized request
            400 - `Page name` and `Slug` should be provided on the request body.
                - Creating a page without providing a `templateId.`
                - Duplicating a page without providing `Page name` and `Slug` on
                  the request body.
            404 - Deleting a non-existent page should return `Resource not found.`


MAIL API
  Location: collections/MailAPISuite.json
  Test:
    1. Emails and Templates
        - Verifies if `/ping` is working.
        - Verifies if the email is sent.
        - Request for the template list and verify if `items` and `meta` are included
          on the response.
        - Verifies if the API Endpoint creates a new mail template.
        - Verifies if the new template can be used and sends out new email.

    2. Negative Test: Mail
        - While not logged-in, this includes a request that creates and sends a
          new message. Expects an `Authorization failed.` message on the response body.
        - Verifies if a request with an invalid json object is rejected. Should expect a response
          body with an error message.
        - Sending an invalid `sourceService` data on the body should return an error
          message `sourceService: must be one of ('splittest', 'integration')`
        - Verifies if the API rejects a request with out a `Content-Type` on the header.
        - Sends an invalid content type on the header. Should expect a response
          body with an error message.

    3. Negative Test: Templates
        - Verifies that requesting a template list unauthorized should be rejected.
        - Sends an invalid content type on the header. Should expect a response
          body with an error message.
        - Verifies if a request with an invalid json object is rejected. Should expect a response
          body with an error message.
        - Verifies if the API rejects a request with out a `Content-Type` on the header.
        - Verifies if the requested non-existent templateId return a 404.

INTEGRATIONS API
  Location: collections/IntegrationsAPISuite.json
  Test:
    1. Ping
        - Pings the API and verifies if the request body has the 'version' data.
    2. Credentials
        - Verifies if the request returns an item list and a meta.
        - Verifies if a credential can be retrieved using UUID and returns its
          corresponding data.
    3. Integrations
        - Verifies if it returns a list of providers and integrations.
        - Verifies if API can create, update and delete integrations.
